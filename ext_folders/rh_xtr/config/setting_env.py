from pathlib import Path

import environ

BASE_DIR = Path(__file__).resolve().parent.parent

env = environ.Env()

env.read_env(Path.joinpath(BASE_DIR, ".env"))


SECRET_KEY = env("SECRET_KEY")


# =================
#  -- IMGIX CDN --
# =================

IX_SKEY = env("IX_SKEY")
SOURCE_ID = env("SOURCE_ID")
PURGE_API_ENDPOINT = env("PURGE_IX_ENDPOINT") + "purge/"
UPLOAD_IX_ENDPOINT = env("SOURCES_IX_ENDPOINT") + SOURCE_ID + "/upload/"
IX_DOMAIN = env("IX_DOMAIN")

# print(PURGE_API_ENDPOINT)

DATABASES = {
    "postgres": {
        "ENGINE": "django.db.backends.postgresql_psycopg2",
        "NAME": env("DATABASE_NAME"),
        "USER": env("DATABASE_USER"),
        "PASSWORD": env("DATABASE_PASSWORD"),
        "HOST": env("DATABASE_HOST"),
        "PORT": env("DATABASE_PORT"),
    },
    "sqlite3": {
        "ENGINE": "django.db.backends.sqlite3",
        "NAME": BASE_DIR / "db.sqlite3",
    },
}

postgres = DATABASES.get("postgres")
sqlite3 = DATABASES.get("sqlite3")
